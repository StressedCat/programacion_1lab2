#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random


def Desordenar(Lista):
    for x in range(0, 6):
        # Se obtiene un numero al azar entre 0 y el tamaño de la Lista
        a = random.randrange(0, len(Lista))
        # se agrega al final de la Lista y luego se borra el que se agrego
        Lista.append(Lista[a])
        del Lista[a]


# Se crea una lista ya predeterminada
Lista = ["La", "mitocondria", "es", "la", "fuente", "de", "poder"]
Desordenar(Lista)
# La lista se invierte al final del programa para despues ser "Printeado"
Lista.reverse()
print(Lista)
