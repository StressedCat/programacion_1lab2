#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def ImprimirTriangulo():
    min = 0
    max = h
    for x in range(0, h):
        print(triangulo[min:max]+trianguloinv[min:max])
        min = min+h
        max = max+h


triangulo = []
trianguloinv = []
h = int(input("Ingrese la altura del triangulo: "))
area = h*h
for x in range(0, h):
    for y in range(0, h):
        # Se llenara la matriz con asteriscos para formar un triangulo
        if x >= y:
            triangulo.append("*")
        else:
            triangulo.append(" ")
        # En el caso del invertido
        # Este se creara para que solo quede invertido verticalmente
        if x <= h-y-1:
            trianguloinv.append("*")
        else:
            trianguloinv.append(" ")
trianguloinv.reverse()
ImprimirTriangulo()
