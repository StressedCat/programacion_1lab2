#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def ordenar():
    for x in range(0, len(numeros)):
        for y in range(0, len(numeros)):
            if numeros[x] <= numeros[y]:
                numeros.append(numeros[y])
                del numeros[y]
    pass


numeros = [5, 2, 1, 4, 3, 6, 7]
# la lista entrara en una repeticion de 100 veces
# asi aumentar la probabilidad de que se ordene
for rep in range(0, 100):
    ordenar()
print(numeros)
